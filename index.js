// console.log("hello");

// Array Methods
// JS has built-in functions and methods for arrays.

//Mutators Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];


//push ()
//Will add element to the end part of the Array.
// SYNTAX arrayName.push()

console.log("Current Array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated Array from push method");
console.log(fruits);

//Adding Multiple elemets to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

//pop();
// Will remove array element to the end part of the array
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// unshift ()
// add element in the beginning of an array

fruits.unshift("Lime", "Banna");
console.log("Mutated array from unshifted method")
console.log(fruits);

//shift();
// removes an element in the beginning part of an array
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

//splice ()
//Simultaneously removes an element from a specified index number and add elements

//SYNTAX -> arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

fruits.splice (1,2, "Lime", "Cherry");
console.log("Mutated array from spice method");
console.log(fruits);

// sort();
// Re-arranges the array elements in alphanumeric order

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse();
// Reverse order of array elements

fruits.reverse();
console.log("Mutated array from the reverse method")
console.log(fruits);

// Non Mutator methods
// Unlike the mutator methods, non mutator methods cannot modify the array.

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf();
// Will return the index number of the first matching element
// If no element was found, JS will return - 1
// Syntax -> arrayName.indexOf(searchValue. fromIndex)

let firstIndex = countries.indexOf("PH");
console.log("Result of index method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of index method: " + invalidCountry);

// lastIndexOf()
// Return index number of the last matching element in an array
// Syntax -> arrayName.lastIndexOf(searchValue. fromIndex)

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

// Getting the index number starting from specified index

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: "+ lastIndexStart);

// slice();
// Portions/slices elements from an array and return a new array.
// Syntax -> arrayName.slice(startingIndex, endingIndex)

let sliceArrayA = countries.slice(2);
console.log("Result from slice method: ")
console.log(sliceArrayA);


let sliceArrayB = countries.slice(2, 4);
console.log("Result from slice method: ")
console.log(sliceArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method");
console.log(slicedArrayC);

// toString()
// Return an array as a string separated by commas
// Syntax -> arrayName.toString()

let stringArray = countries.toString()
console.log("Result from toString method ");
console.log(stringArray);

// concat ()
// combines two arrays and returns the combined result
// Syntax -> arrayA.concat(arrayB)

let taskArray1 = ["drink html", "eat js"];
let taskArray2 = ["inhale css", "breathe sass"];
let taskArray3 = ["get git", "be node"];

let tasks = taskArray1.concat(taskArray2);
console.log("Result from concat method");
console.log(tasks);

// Combining Multiple arrays
console.log("Result from concat method");
let allTasks = taskArray1.concat(taskArray2, taskArray3);
console.log(allTasks);

// Combining arrays with elements
console.log("Result form concat method");
let combinedTasks = taskArray1.concat("smell express", "Throw react");
console.log(combinedTasks);

// join()
// returns an array as a string sperated by specified seperator string
// Syntax -> arrayName.join('separatorString')

let users = ["John", "Jane", "Joe","Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration methods
// forEach
// Similar to a for loop that iterates on each array elements
// Syntax -> arrayName.forEach(function(individualElement)){statement}
allTasks.forEach(function(task) {
	console.log(task);
});

// Using forEach with conditional statement 

let filteredTasks = [];

allTasks.forEach(function(task) {
	if ( task.length > 10 ) {
	filteredTasks.push(task)
	}

})
console.log("Result of filteredTasks ");
console.log(filteredTasks);


// map();
// This is useful for performig tasks where mutating / changing the elements are required.
/*// Syntax -> let/const resultArray = arrayName.map (
function(indiVElement))
*/

let numbers = [1, 2, 3, 4, 5];

// Map also create another array
let numberMap = numbers.map(function(number) {
	return	number * number;

})
console.log("Original Array: ");
console.log(numbers); //Original is unaffected by map();
console.log("Result from map method")
console.log(numberMap); // A new array is returned and stored in a variable.

// map() vs forEach
let numberForEach = numbers.forEach(function (number) {
	return number * number;
	// body...
})
console.log(numberForEach); //undefined

// forEach(), loops over all items in the array as map(), but forEach() does not return new array.


// every()
// check if all elements in an array meet the given condition.
// Syntax -> let/const resultArray = arrayName.every(function(indivElement) {condition})


let allValid = numbers.every(function(number) {
	return (number < 3 );
})
console.log("Result from every method ")
console.log(allValid);

// Some()
// checks if at least one element in array meets the condition
// Syntax -> let / const resultArray = arrayName.some(function(indivElement) {condition})
let someValid = numbers.some(function(number) {
	return (number < 2);
})
console.log("Result from some method");
console.log(someValid);

// filter ();
// Returns new array contains elements which meet the given condition
//  Return an empty array if no elements were found
// Syntax -> let/const resultArray = arrayName.filter(function(indivElement) {condition})

let filterValid = numbers.filter(function(number) {
	return	(number < 3);

})
console.log("Result from filter method");
console.log(filterValid);

// includes()
// Checks if the arguement passed can be found in the array
// Syntax -> arrayName.includes(<argumentToFind>)

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// combine filter and include method
let filteredProduct = products.filter(function(product) {
	return product.toLowerCase().includes("a");
})

console.log(filteredProduct);

// reduce ()
// Evaluates elements from left to right and returns / reduces the array into a single value
// Syntax -> let/const resultArray = arrayName.reduc (function(accumulator, currentValue) {operation})

let iteration = 0;
let reducedArray = numbers.reduce(function(x, y){
	console.log("Current Iteration: " + ++iteration);
	console.log("Accumulator : " + x); 
	console.log("Current Value: " + y);

	// The operation to reduct the array into single value
	return x + y;
})

console.log("Result of reduce method: " + reducedArray);

let list = ["Hello", "Again", "World"];

// adding spaced between elements.
let reducedJoin = list.reduce(function (x, y) {
	return x + " " + y ;
})
console.log("Result of reduced method: "+ reducedJoin);








